package com.drikvy.lore;

import java.util.ArrayList;

import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.drikvy.lore.adapters.NavDrawerListAdapter;
import com.drikvy.lore.data.item.NavDrawerItem;
import com.drikvy.lore.fragments.ComposeLore;
import com.drikvy.lore.fragments.LoresGalleryGrid;
import com.drikvy.lore.fragments.LoresList;
import com.drikvy.lore.fragments.ScrapLore;

/**
 * Lore is your private knowledge sanctuary
 * it shows information
 * that satisfy your interest
 * */
public class MainActivity extends ActionBarActivity implements OnBackStackChangedListener {

	// panel item 0
	private static final int LIST = 0;
	private static final int COMPOSE = 1;
	private int FRAGMENT_COUNT = COMPOSE+1;
	
	// panel item 1
	//private static final int SCRAP = 2;
		
    private Fragment[] fragmentList;
    private Fragment scrapFragment;
    private Fragment scrapList;
	
	/**Drawer Tutorial Fields*/
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
 
    // nav drawer title
    private CharSequence mDrawerTitle;
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
 
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		/**Drawer Tutorial Instantiation*/
		mDrawerTitle = getTitle();
		// load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        // nav drawer icons from resources
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        navDrawerItems = new ArrayList<NavDrawerItem>();
        // adding nav drawer items to array
        // Notes
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        // Lores
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        // Recycle the typed array
        navMenuIcons.recycle();
        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ){
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }
 
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
        
        //TODO this init line has been moved
        fragmentList = new Fragment[FRAGMENT_COUNT];
        fragmentList[LIST] = new LoresList(this);
        fragmentList[COMPOSE] = new ComposeLore(this, null);
        scrapFragment = new ScrapLore(this);
        scrapList = new LoresGalleryGrid(this);
        notesFragmentsInit();
        
        //Listen for changes in the back stack
        getSupportFragmentManager().addOnBackStackChangedListener(this);
		//Handle when activity is recreated like on orientation Change
		shouldDisplayHomeUp();
		
		showFragment(LIST, false);
	}
	
	private void notesFragmentsInit() {
		// TODO Auto-generated method stub
		//FRAGMENT_COUNT = COMPOSE + 1;
		
        FragmentTransaction transaction = 
        		getSupportFragmentManager().beginTransaction();
		
        if(scrapFragment.isVisible())
        	transaction.remove(scrapFragment);
        
        if(fragmentList[LIST].isVisible() || fragmentList[COMPOSE].isVisible()) {
        	
        } else {
        	for(int i = 0; i < FRAGMENT_COUNT; i++) {
            	transaction.add(R.id.main_frame, fragmentList[i]);
            	transaction.hide(fragmentList[i]);
            }
        }
		
        transaction.commit();
	}
	/*
	private void loresFragmentsInit() {
		// TODO Auto-generated method stub
		FRAGMENT_COUNT = SCRAP + 1;
		fragmentList = new Fragment[FRAGMENT_COUNT];
		
        fragmentList[LIST] = new ScrapLore(this);
        FragmentTransaction transaction = 
        		getSupportFragmentManager().beginTransaction();
		
        if(!transaction.isEmpty()) {
        	System.out.println("not empty! MAKE EMPTY!!!");
        	transaction.remove(fragmentList[LIST]);
        	transaction.remove(fragmentList[COMPOSE]);
        }
        
		for(int i = 0; i < FRAGMENT_COUNT; i++) {
        	transaction.add(R.id.main_frame, fragmentList[i]);
        	transaction.hide(fragmentList[i]);
        }
        transaction.commit();
	}
	*/
	
	/**
     * TODO Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            //TODO display view for selected nav drawer item
            Toast.makeText(MainActivity.this, "Drawer: "+position, Toast.LENGTH_SHORT).show();
            if(position == 0) {
            	// reinit notes fragments
            	notesFragmentsInit();
            	showFragment(LIST, false);
            } else {
            	// Insert the fragment by replacing any existing fragment
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                
                if(fragmentList[LIST].isVisible()) {
                	transaction.remove(fragmentList[COMPOSE]);
                } else {
                	transaction.remove(fragmentList[LIST]);
                }
                
                transaction.replace(R.id.main_frame, scrapList)
                               .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                               .commit();
            }
        }
    }
    
    /***
     * TODO Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        //boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        //menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }
    
	@Override
    public void setTitle(CharSequence title) {
		mDrawerTitle = title;
        getActionBar().setTitle(mDrawerTitle);
    }
	
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
	
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
	
	public ActionBarDrawerToggle getmDrawerToggle() {
		return mDrawerToggle;
	}

	@Override
	protected void onPause() {
		super.onPause();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		// showFragment(LIST, false);
	}
	
	@Override
	public void onBackStackChanged() {
	    shouldDisplayHomeUp();
	}

	public void showComposeFragment() {
		openSavedLore(null);
	}
	
	public void shouldDisplayHomeUp(){
		//Enable Up button only  if there are entries in the back stack
		boolean canback = getSupportFragmentManager().getBackStackEntryCount()>0;
		getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
	}

	@Override
	public boolean onSupportNavigateUp() {
	    //This method is called when the up button is pressed. Just the pop back stack.
	    getSupportFragmentManager().popBackStack();
	    fragmentList[LIST].onResume();
	    
	    return true;
	}
	
	private void showFragment(int fragmentIndex, boolean addToBackStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        for (int i = 0; i < fragmentList.length; i++) {
            if (i == fragmentIndex) {
                transaction.show(fragmentList[i]);
            } else {
                transaction.hide(fragmentList[i]);
            }
        }
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

	public void openSavedLore(String date) {
		// TODO Auto-generated method stub
		FragmentTransaction transaction = 
        		getSupportFragmentManager().beginTransaction();
		transaction.remove(fragmentList[COMPOSE]);
		
		fragmentList[COMPOSE] = new ComposeLore(this, date);
		transaction.add(R.id.main_frame, fragmentList[COMPOSE]);
		transaction.hide(fragmentList[COMPOSE]);
		transaction.commit();
		
		showFragment(COMPOSE, true);
	}
	
}