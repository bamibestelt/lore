package com.drikvy.lore.fragments;

import java.util.ArrayList;
import java.util.Arrays;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.drikvy.lore.MainActivity;
import com.drikvy.lore.R;
import com.drikvy.lore.adapters.LoresListAdapter;
import com.drikvy.lore.adapters.LoresListAdapter.ViewHolder;
import com.drikvy.lore.data.item.LoreData;
import com.drikvy.lore.db.LoreDbHelper;
import com.haarman.listviewanimations.itemmanipulation.OnDismissCallback;
import com.haarman.listviewanimations.itemmanipulation.SwipeDismissAdapter;

public class LoresList extends Fragment implements OnDismissCallback {

	private ListView lvLores;
	
	private MainActivity activity;
	private LoresListAdapter listAdapter;
	private ArrayList<Object> listData = new ArrayList<Object>();
	
	public LoresList(MainActivity activity) {
    	this.activity = activity;
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_compose);
		/*
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
		     // only for gingerbread and newer versions
			//getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		} else {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
		*/
		setHasOptionsMenu(true);

	    // If your minSdkVersion is 11 or higher, instead use:
	    // getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_lores_list, null, false);
		
		lvLores = (ListView) view.findViewById(R.id.lv_saved_lores);
		listAdapter = new LoresListAdapter(activity, listData);
		//lvLores.setAdapter(listAdapter);
		
		setSwipeDismissAdapter();
		
		return view;
	}
	
	private void setSwipeDismissAdapter() {
		SwipeDismissAdapter adapter = new SwipeDismissAdapter(listAdapter, this);
		adapter.setAbsListView(lvLores);
		lvLores.setAdapter(adapter);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		LoreDbHelper database = new LoreDbHelper(activity);
		
		listData.clear();
		listData.addAll(database.getLores());
		
		listAdapter.notifyDataSetChanged();
		
		lvLores.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> av, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				ViewHolder holder = (ViewHolder) view.getTag();
				String date = holder.txtDate.getText().toString();
				System.out.println("ONCLICK: "+date);
				
				activity.openSavedLore(date);
			}
		});
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		
		inflater.inflate(R.menu.main, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
        if (activity.getmDrawerToggle().onOptionsItemSelected(item)) {
            return true;
        }
		
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_new_lore:
	            
	        	activity.showComposeFragment();
	        	return true;
	        
	            /*
	        case R.id.action_settings:
	            
	            return true;
	            */
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	@Override
	public void onDismiss(AbsListView listView, int[] reverseSortedPositions) {
		// TODO Auto-generated method stub
		for (int position : reverseSortedPositions) {
			LoreData data = (LoreData) listAdapter.getItem(position);
			listAdapter.remove(position);
			
			new DeleteAsyncFromDb().execute(data.getDate());
		}
		Toast.makeText(activity, "Removed positions: " + Arrays.toString(reverseSortedPositions), Toast.LENGTH_SHORT).show();
	}
	
	private class DeleteAsyncFromDb extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... params) {
			LoreDbHelper database = new LoreDbHelper(activity);
			database.deleteNoteByDate(params[0]);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}
	}
		
}