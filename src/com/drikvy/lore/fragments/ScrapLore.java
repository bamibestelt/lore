package com.drikvy.lore.fragments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import yuku.ambilwarna.AmbilWarnaDialog;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.drikvy.lore.CustomCanvas;
import com.drikvy.lore.MainActivity;
import com.drikvy.lore.R;

public class ScrapLore extends Fragment {

	private MainActivity activity;
	private CustomCanvas custom;
	private View view;
		
	public ScrapLore(MainActivity activity) {
    	this.activity = activity;
    	
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_lore_scrap, null, false);
		
		custom = new CustomCanvas(activity);
		//view.setDrawingCacheEnabled(true);
		//view.setDrawingCacheQuality(LinearLayout.DRAWING_CACHE_QUALITY_HIGH);
		
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		custom.setLayoutParams(params);
		((ViewGroup) view).addView(custom);		
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//System.out.println("Scraplore onPause");
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		//System.out.println("Scraplore onDestroyView");
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		//System.out.println("Scraplore onDestroy");
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		
		inflater.inflate(R.menu.scrap, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_choose_colour_lore: 
			openDialog(true);
			break;
		case R.id.action_save_image:
			defineFileName();
			break;	
        }
		return super.onOptionsItemSelected(item);
	}
	
	private void defineFileName() {
		// TODO Auto-generated method stub
		Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage("Insert filename");
		builder.setCancelable(true);
		
		final EditText ed = new EditText(activity);
		builder.setView(ed);
		
		builder.setPositiveButton("Ok", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				String filename = ed.getText().toString();
				getScreenCap(view, filename);
			}
		});
		builder.setNegativeButton("Cancel", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
		
	}

	void openDialog(boolean supportsAlpha) {
		AmbilWarnaDialog dialog = new AmbilWarnaDialog(activity, Color.GREEN, supportsAlpha, new AmbilWarnaDialog.OnAmbilWarnaListener() {
			@Override
			public void onOk(AmbilWarnaDialog dialog, int color) {
				custom.setBrushColor(color);
			}

			@Override
			public void onCancel(AmbilWarnaDialog dialog) {
				
			}
		});
		dialog.show();
	}
	
	void getScreenCap(View yourView, String filename) {
	    yourView.setDrawingCacheEnabled(true);
	    yourView.setDrawingCacheQuality(LinearLayout.DRAWING_CACHE_QUALITY_HIGH);
	    yourView.buildDrawingCache();
	    Bitmap bmp = null;
	    if (yourView != null) { 
	        try {
	            bmp = Bitmap.createBitmap(yourView.getDrawingCache());
	        } catch (NullPointerException e) { }
	    }
	    yourView.setDrawingCacheEnabled(false);
	    yourView.destroyDrawingCache();

	    if (bmp != null) {
	    	String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/lore";
	        File dir = new File(file_path);
	        if (!dir.exists()) { 
	            dir.mkdirs();
	        }
	        File file = new File(dir, filename+".png");
	        
	        FileOutputStream fOut = null;
	        try { 
	        		fOut = new FileOutputStream(file);
	        		bmp.compress(Bitmap.CompressFormat.PNG, 85, fOut);
	        		
	        		fOut.flush(); 
		        	fOut.close(); 
		        	Toast.makeText(activity, "Image saved!", Toast.LENGTH_SHORT).show();
	        } catch (FileNotFoundException e1) { 
	        		Toast.makeText(activity, "File not found?", Toast.LENGTH_SHORT).show();
	        } catch (IOException e1) { 
	        		Toast.makeText(activity, "Failed saving image!", Toast.LENGTH_SHORT).show();
	        }
	    }
	}
	
}