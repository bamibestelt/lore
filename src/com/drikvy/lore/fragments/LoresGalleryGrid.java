package com.drikvy.lore.fragments;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.drikvy.lore.MainActivity;
import com.drikvy.lore.R;
import com.haarman.listviewanimations.ArrayAdapter;
import com.haarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;

public class LoresGalleryGrid extends Fragment {

	private MainActivity activity;
	private ArrayList<SimpleData> listFiles = new ArrayList<SimpleData>();
	
	public LoresGalleryGrid(MainActivity activity) {
    	this.activity = activity;
    	
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getListOfFiles();
	}
	
	void getListOfFiles() {
		String path = Environment.getExternalStorageDirectory().toString()+"/lore";
		File f = new File(path);
		File file[] = f.listFiles();
		if(file != null) {
			for(int i=0; i<file.length; i++) {
				Log.d("Files", "FileName:"+ file[i].getAbsolutePath());
				
				String pathName = file[i].getAbsolutePath();
				
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig = Bitmap.Config.ARGB_8888;
				Bitmap bitmap = BitmapFactory.decodeFile(pathName, options);
				
				String id = String.valueOf(System.currentTimeMillis());
				listFiles.add(new SimpleData(id, bitmap));
			}
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.activity_gridview, null, false);
		
		GridView gridView = (GridView) view.findViewById(R.id.activity_gridview_gv);
		SwingBottomInAnimationAdapter swingBottomInAnimationAdapter 
			= new SwingBottomInAnimationAdapter(new MyAdapter(activity, listFiles));
		swingBottomInAnimationAdapter.setAbsListView(gridView);
		swingBottomInAnimationAdapter.setInitialDelayMillis(300);
		gridView.setAdapter(swingBottomInAnimationAdapter);
		
		return view;
	}
	
		
	private static class MyAdapter extends ArrayAdapter<SimpleData> {

		private Context mContext;
		private ArrayList<SimpleData> localData;
		private LruCache<String, Bitmap> mMemoryCache;

		public MyAdapter(Context context, ArrayList<SimpleData> list) {
			super(list);
			mContext = context;
			localData = list;
			final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

			// Use 1/8th of the available memory for this memory cache.
			final int cacheSize = maxMemory;
			mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
				@Override
				protected int sizeOf(String key, Bitmap bitmap) {
					// The cache size will be measured in kilobytes rather than
					// number of items.
					return bitmap.getRowBytes() * bitmap.getHeight() / 1024;
				}
			};
		}

		@Override
		public View getView(int position, View convertView, ViewGroup viewGroup) {
			ImageView imageView = (ImageView) convertView;

			if (imageView == null) {
				imageView = new ImageView(mContext);
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			}

			/*
			int imageResId;
			switch (getItem(position) % 5) {
			case 0:
				imageResId = R.drawable.img_nature1;
				break;
			case 1:
				imageResId = R.drawable.img_nature2;
				break;
			case 2:
				imageResId = R.drawable.img_nature3;
				break;
			case 3:
				imageResId = R.drawable.img_nature4;
				break;
			default:
				imageResId = R.drawable.img_nature5;
			}
			 */
			SimpleData data = localData.get(position);
			
			Bitmap bitmap = getBitmapFromMemCache(data.getKey());
			if (bitmap == null) {
				bitmap = data.getBitmap();//BitmapFactory.decodeResource(mContext.getResources(), imageResId);
				addBitmapToMemoryCache(data.getKey(), bitmap);
			}
			imageView.setImageBitmap(bitmap);

			return imageView;
		}

		private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
			if (getBitmapFromMemCache(key) == null) {
				mMemoryCache.put(key, bitmap);
			}
		}

		private Bitmap getBitmapFromMemCache(String key) {
			return mMemoryCache.get(key);
		}
	}
	
	private class SimpleData {
		private String key;
		private Bitmap bitmap;
		
		public SimpleData(String id, Bitmap image) {
			this.key = id;
			this.bitmap = image;
		}
		
		public String getKey() {
			return key;
		}
		
		public Bitmap getBitmap() {
			return bitmap;
		}
		
	}
}
