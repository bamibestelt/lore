package com.drikvy.lore.fragments;

import java.text.DateFormat;
import java.util.Date;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.drikvy.lore.R;
import com.drikvy.lore.data.item.LoreData;
import com.drikvy.lore.db.LoreDbHelper;

public class ComposeLore extends Fragment {

	private ActionBarActivity activity;
	private String date, title, content = "";
	private String savedDate;
	
	private EditText edLoreTitle;
	private EditText edLoreTexts;
	
	public ComposeLore(ActionBarActivity activity, String date) {
    	this.activity = activity;
    	this.savedDate = date;
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_compose);
		/*
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
		     // only for gingerbread and newer versions
			//getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		} else {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
		*/
		setHasOptionsMenu(true);
		
	    // If your minSdkVersion is 11 or higher, instead use:
	    // getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_lore_compose, null, false);
		
		edLoreTitle = (EditText) view.findViewById(R.id.ed_lore_title);
		edLoreTexts = (EditText) view.findViewById(R.id.ed_lore_box);
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		/*
		ActionBar ab = activity.getSupportActionBar();
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(true);*/
		
		if(savedDate != null) {
			// TODO load lore from db by date
			LoreDbHelper database = new LoreDbHelper(activity);
			LoreData lore = database.getLoreByDate(savedDate);
			
			if(lore != null) {
				edLoreTitle.setText(lore.getTitle());
				edLoreTexts.setText(lore.getContent());
			}
		}
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		
		inflater.inflate(R.menu.compose, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
	    
        case R.id.action_save_lore:
            // save here
        	date = DateFormat
				.getDateTimeInstance().format(new Date());
        	System.out.println("DATE-FORMAT: "+date);
        	
        	title = edLoreTitle.getText().toString();
        	content = edLoreTexts.getText().toString();
        	
        	LoreDbHelper database = new LoreDbHelper(activity);
        	database.saveLore(date, title, content, savedDate);
        	
            return true;
        default:
            return super.onOptionsItemSelected(item);
		}
	}
}