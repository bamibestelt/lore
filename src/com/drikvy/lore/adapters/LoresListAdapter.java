package com.drikvy.lore.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.drikvy.lore.R;
import com.drikvy.lore.data.item.LoreData;

public class LoresListAdapter extends BaseAdapter {

	private static final String TAG = "LoresListAdapter";
	private Activity activity;
	private ArrayList<Object> listData;
	private static LayoutInflater inflater = null;
	
	public LoresListAdapter(Activity a, ArrayList<Object> data) {
		this.activity = a;
		this.listData = data;

		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void clear() {
		listData.clear();
	}

	public void addAll(ArrayList<Object> data) {
		listData.addAll(data);
	}

	
	public int getCount() {
		return listData.size();
	}

	public Object getItem(int position) {
		// return position;
		return listData.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	
	public static class ViewHolder {
		public TextView txtDate;
		public TextView txtTitle;
		public TextView txtTexts;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();

			vi = inflater.inflate(R.layout.list_item_saved_lores, null);
			holder.txtDate = (TextView) vi.findViewById(R.id.tv_saved_lore_date);
			holder.txtTitle = (TextView) vi.findViewById(R.id.tv_saved_lore_title);
			holder.txtTexts = (TextView) vi.findViewById(R.id.tv_saved_lore_text);
			
			vi.setTag(holder);
		} else {
			holder = (ViewHolder) vi.getTag();
		}
		
		LoreData lore = (LoreData) listData.get(position);
		
		holder.txtDate.setText(lore.getDate());
		holder.txtTitle.setText(lore.getTitle());
		holder.txtTexts.setText(lore.getContent());
			
		return vi;
	}

	public void remove(int position) {
		// TODO Auto-generated method stub
		listData.remove(position);
		notifyDataSetChanged();
	}
	
}