package com.drikvy.lore.db;

import android.provider.BaseColumns;

public final class LoreDbContract {

	// To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public LoreDbContract() {}

    /* Inner class that defines the table contents */
    public static abstract class Repository implements BaseColumns {
        public static final String TABLE_NAME = "repository";
        //public static final String COLUMN_ID = "id";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_CONTENT = "content";
    }
    
}
