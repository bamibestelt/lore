package com.drikvy.lore.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.drikvy.lore.data.item.LoreData;
import com.drikvy.lore.db.LoreDbContract.Repository;


public class LoreDbHelper extends SQLiteOpenHelper {

	private static final String TAG = "LoreDbHelper";
	
	// If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "lores.db";

    private static final String VARCHAR_TYPE = " VARCHAR";
    private static final String BLOB_TYPE = " BLOB";
    private static final String COMMA_SEP = ",";
    
    private long milResult = 0;
	private Context appContext;
    
    private static final String SQL_CREATE_BANGO_ENTRIES =
        "CREATE TABLE " + Repository.TABLE_NAME + " (" +
        		Repository.COLUMN_DATE + VARCHAR_TYPE + "(" + 30 + ")" + COMMA_SEP +
        		Repository.COLUMN_TITLE + VARCHAR_TYPE + "(" + 40 + ")" + COMMA_SEP +
        		Repository.COLUMN_CONTENT + VARCHAR_TYPE + "(" + 500 + ")" + " )";

    private static final String SQL_DELETE_ENTRIES =
        "DROP TABLE IF EXISTS " + Repository.TABLE_NAME;
    
    public LoreDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        appContext = context;
    }
    
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_BANGO_ENTRIES);
    }
    
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**save new Lore
     * @param savedDate 
     * @param creator */
    public void saveLore(String date, String title, String content, String savedDate) {
    	
    	SQLiteDatabase db = this.getWritableDatabase();
    	
    	// Create a new map of values, where column names are the keys
    	ContentValues values = new ContentValues();
    	values.put(Repository.COLUMN_DATE, date);
    	values.put(Repository.COLUMN_TITLE, title);
    	values.put(Repository.COLUMN_CONTENT, content);

    	// Insert the new row, returning the primary key value of the new row
    	if(savedDate != null) {
    		long totalAffectedRow;
        	totalAffectedRow = db.update(Repository.TABLE_NAME
        			, values, Repository.COLUMN_DATE+"=?", new String[]{savedDate});
        	Log.v(TAG, "totalAffectedRow: "+totalAffectedRow);
        } else {
    		long newRowId = db.insert(Repository.TABLE_NAME, null, values);
    		Log.v(TAG, "newRowId: "+newRowId);
    	}
    	
    	// TODO add toast
    	Toast.makeText(appContext, "Lore saved!", Toast.LENGTH_SHORT).show();
    }
    
    /**get all saved Lores*/
    public ArrayList<Object> getLores() {

    	ArrayList<Object> list 
    		= new ArrayList<Object>();
    	
    	SQLiteDatabase db = this.getReadableDatabase();

    	// Define a projection that specifies which columns from the database
    	// you will actually use after this query.
    	String[] projection = {
    			Repository.COLUMN_DATE,
    			Repository.COLUMN_TITLE,
    			Repository.COLUMN_CONTENT};

    	Cursor c = db.query(
    		true,
    		Repository.TABLE_NAME,  			// The table to query
    	    projection,                         // The columns to return
    	    null,       						// The columns for the WHERE clause
    	    null,                            	// The values for the WHERE clause
    	    null,      // don't group the rows
    	    null,                               // don't filter by row groups
    	    null,								// The sort order
    	    null								// LIMIT
    	    );
    	
    	c.moveToFirst();
    	int total = 0;
    	while(!c.isAfterLast()) {
    		String date = c.getString(0);
    		String title = c.getString(1);
    		String content = c.getString(2);
    					
    		LoreData data = new LoreData(date, title, content);
    		
			// TODO add to list
			list.add(data);
			
    		total++;
    		c.moveToNext();
    	}
    	
    	c.close();
    	//this.close();
    	Log.v(TAG, "total: "+total);
    	
    	return list;
    }
    
    /**get saved Lore by date*/
    public LoreData getLoreByDate(String savedDate) {

    	LoreData data = null;
    	
    	SQLiteDatabase db = this.getReadableDatabase();

    	// Define a projection that specifies which columns from the database
    	// you will actually use after this query.
    	String[] projection = {
    			Repository.COLUMN_DATE,
    			Repository.COLUMN_TITLE,
    			Repository.COLUMN_CONTENT};

    	Cursor c = db.query(
    		true,
    		Repository.TABLE_NAME,  			// The table to query
    	    projection,                         // The columns to return
    	    Repository.COLUMN_DATE+"=?",       						// The columns for the WHERE clause
    	    new String[]{savedDate},                            	// The values for the WHERE clause
    	    null,      // don't group the rows
    	    null,                               // don't filter by row groups
    	    null,								// The sort order
    	    null								// LIMIT
    	    );
    	
    	c.moveToFirst();
    	if(c.getCount() != 0) {
    		String date = c.getString(0);
    		String title = c.getString(1);
    		String content = c.getString(2);
    					
    		data = new LoreData(date, title, content);
    	}
    	
    	c.close();
    	
    	return data;
    }

	public void deleteNoteByDate(String date) {
		// TODO Auto-generated method stub
		SQLiteDatabase db = this.getWritableDatabase();
    	
    	String whereClause = Repository.COLUMN_DATE+"=?";
    	String[] whereArgs = new String[] {date};

    	db.delete(Repository.TABLE_NAME, whereClause, whereArgs);
    	this.close();
	}
   
}